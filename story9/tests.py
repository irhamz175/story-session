from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *

class uniTests(TestCase):

    def test_ada_url(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_ada_fungsi(self):
        response = resolve("/")
        self.assertEqual(response.func,index)

    def test_ada_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,"index.html")

    def test_ada_button(self):
        response = Client().get("")
        content = response.content.decode("utf8")
        self.assertIn("</button",content)
